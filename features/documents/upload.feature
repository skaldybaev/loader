Feature: Upload
  In order to share my files to other
  As a user
  I want to upload files

  Background:
    Given I am logged in

  Scenario: Successful upload
    When I upload with valid data
    Then I should see a successful status message

  Scenario: Uploading without title
    When I upload with invalid data
    Then I should see a missing title message