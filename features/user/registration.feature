Feature: Registration
  In order to use Loader application
  As a user
  I want to be able to register

  Background:
    Given I am not logged in

  Scenario: Successful registration
    When I register with valid user data
    Then I should see a successful registration message

  Scenario: Registration with invalid email
    When I register with an invalid email
    Then I should see an invalid email message

  Scenario: Registration without password
    When I register without a password
    Then I should see a missing password message

  Scenario: Registration without password confirmation
    When I register without a password confirmation
    Then I should see a missing password confirmation message

  Scenario: Registration with mismatched password and confirmation
    When I register with a mismatched password confirmation
    Then I should see a mismatched password message