Feature: Login
  In order to be able to upload files
  A user
  Should be able to login

  Scenario: User is not registered
    When I login with valid credentials
    Then I should see an invalid login message
    And I should be logged out

  Scenario: User successfully login
    Given I exist as a user
    And I am not logged in
    When I login with valid credentials
    Then I should see a successful login message
    And I should be logged in

  Scenario: User enters wrong email
    Given I exist as a user
    And I am not logged in
    When I login with a wrong email
    Then I should see an invalid login message
    And I should be logged out

  Scenario: User enters wrong password
    Given I exist as a user
    And I am not logged in
    When I login with a wrong password
    Then I should see an invalid login message
    And I should be logged out