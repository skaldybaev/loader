def create_visitor
  @visitor ||= {email: 'test@gmail.com', password: 'superpassword', password_confirmation: 'superpassword'}
end

def find_user
  @user ||= User.where(email: @visitor[:email]).first
end

def delete_user
  @user ||= User.where(email: @visitor[:email]).first
  @user.destroy unless @user.nil?
end

def create_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:user, @visitor)
end

def register
  delete_user
  visit '/users/sign_up'
  fill_in 'user_email', with: @visitor[:email]
  fill_in 'user_password', with: @visitor[:password]
  fill_in 'user_password_confirmation', with: @visitor[:password_confirmation]
  click_button 'Зарегистрироваться'
  find_user
end

def login
  visit '/users/sign_in'
  fill_in 'user_email', with: @visitor[:email]
  fill_in 'user_password', with: @visitor[:password]
  click_button 'Войти'
end

# Background

Given /^I am not logged in$/ do
  visit '/users/sign_out'
end

When(/^I register with valid user data$/) do
  create_visitor
  register
end

When(/^I register with an invalid email$/) do
  create_visitor
  @visitor = @visitor.merge(email: 'notanemail')
  register
end

When(/^I register without a password$/) do
  create_visitor
  @visitor = @visitor.merge(password: '')
  register
end

When(/^I register without a password confirmation$/) do
  create_visitor
  @visitor = @visitor.merge(password: 'superpassword', password_confirmation: '')
  register
end

When(/^I register with a mismatched password confirmation$/) do
  create_visitor
  @visitor = @visitor.merge(password: 'superpassword', password_confirmation: 'superpasswor')
  register
end

When(/^I login with valid credentials$/) do
  create_visitor
  login
end

Then(/^I should be logged out$/) do
  expect(page).to have_content('Войти')
end

Then(/^I should be logged in$/) do
  expect(page).to have_content('Выйти')
end

Given(/^I exist as a user$/) do
  create_user
end

When(/^I login with a wrong email$/) do
  @visitor = @visitor.merge(email: 'wrong@gmail.com')
  login
end

When(/^I login with a wrong password$/) do
  @visitor = @visitor.merge(password: 'superpasswor')
  login
end

Given(/^I am logged in$/) do
  create_user
  login
end

When(/^I logout$/) do
  visit '/users/sign_out'
end

When(/^I return to the site$/) do
  visit '/'
end

Then(/^I should see a logged out message$/) do
  expect(page).to have_content('Выход из системы выполнен.')
end

Then(/^I should see a successful login message$/) do
  expect(page).to have_content('Вход в систему выполнен.')
end

Then(/^I should see an invalid login message$/) do
  expect(page).to have_content('Неверный адрес e-mail или пароль.')
end

Then(/^I should see a mismatched password message$/) do
  expect(page).to have_content(/не совпадает с Пароль/i)
end

Then(/^I should see a missing password confirmation message$/) do
  expect(page).to have_content(/не совпадает с Пароль/i)
end

Then(/^I should see a missing password message$/) do
  expect(page).to have_content(/не может быть пустым/i)
end

Then(/^I should see an invalid email message$/) do
  expect(page).to have_content(/имеет неверное значение/i)
end

Then(/^I should see a successful registration message$/) do
  expect(page).to have_content 'Добро пожаловать! Вы успешно зарегистрировались.'
end
