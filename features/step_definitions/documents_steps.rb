def create_document
  @document ||= {title: 'Test title', tag_list: 'tag1, tag2', file: "#{Rails.root}/spec/fixtures/car.jpg"}
end

def upload
  visit '/documents/new'
  fill_in 'document_title', with: @document[:title]
  fill_in 'document_tag_list', with: @document[:tag_list]
  attach_file 'upload-field', @document[:file]
  click_button 'Сохранить'
end

When(/^I upload with valid data$/) do
  create_document
  upload
end

When(/^I upload with invalid data$/) do
  create_document
  @document = @document.merge(title: '')
  upload
end

Then(/^I should see a successful status message$/) do
  expect(page).to have_content(/success/i)
end

Then(/^I should see a missing title message$/) do
  page.should have_content /не может быть пустым/i
end
