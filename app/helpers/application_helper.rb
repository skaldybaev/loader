module ApplicationHelper
  def nav_link(link_text, path)
    class_name = 'active' if current_page? path
    content_tag(:li, class: class_name) do
      link_to link_text, path
    end
  end

  def icon(name)
    content_tag(:span, nil, class: "glyphicon glyphicon-#{name}")
  end

  def flash_class(level)
    case level
      when 'notice' then 'success'
      when 'error' then 'danger'
      when 'alert' then 'danger'
    end
  end

  def tag_cloud(tags, classes)
    max = tags.sort_by(&:count).last
    tags.each do |tag|
      index = tag.count.to_f / max.count * (classes.size - 1)
      yield(tag, classes[index.round])
    end
  end

  def file_tag doc, option=nil
    if doc.file_processing?
      image_tag '189x142.gif', class: "img-rounded #{option}"
    elsif doc.file_url.match(/jpeg|png|jpg|gif/)
      image_tag doc.file_url(:thumb), class: "img-rounded #{option}"
    else
      image_tag '189x142.gif', class: "img-rounded #{option}"
    end
  end
end
