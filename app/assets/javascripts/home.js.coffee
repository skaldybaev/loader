# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $("#fileupload").fileupload
    paramName: "document[file]"
    maxNumberOfFiles: 1

    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.progress-bar')
          .attr('aria-valuenow', progress)
          .css('width', progress + '%')
    done: (e, data) ->
      window.location = data.url