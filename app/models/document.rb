class Document < ActiveRecord::Base
  mount_uploader :file, FileUploader
  process_in_background :file
  validates :title, :file, presence: true

  belongs_to :user
  has_many :taggings
  has_many :tags, through: :taggings

  def to_jq_upload
    {
        name: read_attribute(:file),
        size: file.size,
        url: file.url,
        delete_url: "/documents/#{id}",
        delete_type: 'DELETE'
    }
  end

  def self.tagged_with name
    Tag.find_by(name: name).documents
  end

  def self.tag_counts
    Tag.select("tags.*, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id")
  end

  def tag_list
    tags.map(&:name).join(", ")
  end

  def tag_list=(names)
    self.tags = names.split(',').map do |n|
      Tag.where(name: n.strip).first_or_create!
    end if names
  end
end
