# encoding: utf-8
class FileUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes
  include ::CarrierWave::Backgrounder::Delay
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def move_to_cache
    true
  end

  def move_to_store
    true
  end

  process :set_content_type

  version :thumb, if: :image? do
    process :resize_to_fill => [189,142]
  end

  protected

  def image?(new_file)
    new_file.content_type.start_with? 'image'
  end
end
