class HomeController < ApplicationController
  def index
    @documents = if params[:tag]
      Document.tagged_with(params[:tag])
    else
      Document.all
    end
  end
end
