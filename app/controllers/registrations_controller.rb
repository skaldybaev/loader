class RegistrationsController < Devise::RegistrationsController
  def new
    add_breadcrumb 'Регистрация', :new_user_registration_path
    super()
  end
end