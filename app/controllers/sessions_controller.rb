class SessionsController < Devise::SessionsController
  def new
    add_breadcrumb 'Войти', :new_user_session_path
    super()
  end
end