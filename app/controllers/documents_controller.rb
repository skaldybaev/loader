class DocumentsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  before_filter :find_document, only: [:show, :destroy]
  add_breadcrumb 'Мои файлы', :documents_path

  respond_to :html, :json

  def index
    @documents = current_user.documents
  end

  def new
    add_breadcrumb 'Загрузить', :new_documents_path
    @document = current_user.documents.new

    respond_to do |format|
      format.html
      format.json { render json: @document }
    end
  end

  def create
    @document = current_user.documents.new(document_params)

    if @document.save
      render json: {file: @document.to_jq_upload, url: documents_path, status: 'success'}, status: :created, location: @document
    else
      render json: @document.errors, status: :unprocessable_entity
    end
  end

  def show
    respond_modal_with @document
  end

  def destroy
    @document.destroy
    redirect_to documents_path, notice: 'Файл успешно удален'
  end

  private

  def document_params
    params.require(:document).permit(:title, :file, :tag_list)
  end

  def find_document
    @document = Document.find(params[:id])
  end
end
