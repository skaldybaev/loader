require 'sidekiq/web'

Rails.application.routes.draw do
  root to: 'home#index'

  devise_for :users, controllers: {sessions: 'sessions', registrations: 'registrations'}
  resources :documents
  get 'tags/:tag', to: 'home#index', as: :tag

  mount Sidekiq::Web, at: '/sidekiq'
end
