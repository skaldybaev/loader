class AddFileProcessingToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :file_processing, :boolean, null: false, default: false
  end
end

