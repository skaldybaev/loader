# Read about factories at https://github.com/thoughtbot/factory_girl
include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :document do
    title 'Test title'
    file { fixture_file_upload(Rails.root.join('spec', 'fixtures', 'car.jpg'), 'image/jpg') }
    tag_list 'tag1, tag2'
  end
end