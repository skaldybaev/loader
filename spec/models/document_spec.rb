require 'spec_helper'

describe Document do
  let(:document) { FactoryGirl.create(:document) }

  it { expect(document).to be_valid }
  it { expect(document.tag_list).to eq('tag1, tag2') }

  it { is_expected.to have_many(:tags).through(:taggings) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:file) }
end
